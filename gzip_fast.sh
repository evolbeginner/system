#! /bin/bash

#############################################
while [ $# -gt 0 ]; do
	case $1 in
		-i|--in|--input)
			inputs=$2
			shift
			;;
		-o|--out|--output)
			outputs=$2
			shift
			;;
		--rm|remove)
			is_remove_input=1
			;;
		*)
			echo "Illegal param! Exiting ......"
			exit
	esac
	shift;
done


#############################################
for input in ${inputs[@]}; do
	output=${input%/}.gz
	gzip -c $input > $output
	echo $input
	if [ $? -eq 0 ]; then
		if [ ! -z $is_remove_input ]; then
			rm $input
		fi
	fi
done


