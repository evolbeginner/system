#! /bin/bash

for i in $@; do
	tar vcfj ${i%/}.tar.bz2 $i
done
